﻿using SmartTaxiSystem.Context;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class Owner
    {
        public int OwnerID { get; set; }
        
        [Required]
        public string OwnerName { get; set; }
        
        [Required]
        public string ContactNo { get; set; }
        
        public string Address { get; set; }
        public string FaxNo { get; set; }
        //public ICollection<Employee> Employees { get; set; }

        public static object GetOwners()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from Owner in DB.Owners
                        select new
                        {
                            OwnerId = Owner.OwnerID,
                            OwnerName = Owner.OwnerName
                        };

            return query.ToList();
        }
    }
}