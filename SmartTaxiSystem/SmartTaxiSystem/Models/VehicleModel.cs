﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class VehicleModel
    {
        public int VehicleModelID { get; set; }

        [Required]
        public string Descr { get; set; }

        [Required]
        public int VehicleMakeID { get; set; }

        [Required]
        public int VehicleTypeID { get; set; }

        public static object GetVehicleModels()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from VehicleModel in DB.VehicleModels
                        select new
                        {
                            VehicleModelID = VehicleModel.VehicleModelID,
                            Descr = VehicleModel.Descr
                        };
            return query.ToList();
        }
    }
}