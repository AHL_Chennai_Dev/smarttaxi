﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartTaxiSystem.Enums;
using System.ComponentModel.DataAnnotations;

namespace SmartTaxiSystem.Models
{
    public class BookingCancel
    {
        public int BookingCancelID { get; set; }
        public int BookingID { get; set; }
        public Booking Booking { get; set; }
        public BookingStatus Status { get; set; }
        public BookingCancelledBy CancelledBy { get; set; }
        public string CancelReason { get; set; }
        public int VehicleID { get; set; }
        public Vehicle Vehicle { get; set; }
        public int EmployeeID { get; set; }
        public Employee Employee { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Created Time")]
        public DateTime CreatedTime { get; set; }
        [Display(Name = "Updated By")]
        public string UpdateDetailsUpdatedBy { get; set; }
        [Display(Name = "Updated Time")]
        public DateTime UpdateDetailsUdpatedTime { get; set; }
    }
}