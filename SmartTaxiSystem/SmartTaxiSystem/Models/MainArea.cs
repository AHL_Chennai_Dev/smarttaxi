﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class MainArea
    {
        public int MainAreaID { get; set; }
        public string MainAreaName { get; set; }

        public static object GetMainAreas()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from MainArea in DB.MainAreas
                        select new
                        {
                            MainAreaId = MainArea.MainAreaID,
                            MainAreaName = MainArea.MainAreaName
                        };

            return query.ToList();
        }
    }
}