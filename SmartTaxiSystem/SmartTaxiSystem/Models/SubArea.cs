﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class SubArea
    {
        public int SubAreaID { get; set; }
        public int MainAreaID { get; set; }
        public MainArea MainArea { get; set; }
        public string SubAreaName { get; set; }

        public static object GetSubAreas()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from SubArea in DB.SubAreas
                        select new
                        {
                            SubAreaId = SubArea.SubAreaID,
                            SubAreaName = SubArea.SubAreaName
                        };

            return query.ToList();
        }
    }
}