﻿using SmartTaxiSystem.Context;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class Role
    {
        public int RoleID { get; set; }
        public string Title { get; set; }

        //public ICollection<Employee> Employees { get; set; }

        public static object GetRoles()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from Role in DB.Roles
                        select new
                        {
                            RoleID = Role.RoleID,
                            Title = Role.Title
                        };

            return query.ToList();
        }
    }
}