﻿using DevExpress.Xpo;
using SmartTaxiSystem.Context;
using SmartTaxiSystem.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class Vehicle
    {
        public int VehicleID { get; set; }

        [Required]
        [DisplayName("Active Status")]
        public VehicleActiveStatus ActiveStatus { get; set; }

        //[Required]
        //public VehicleColor VehicleColor { get; set; }

        [Required]
        [DisplayName("IMEI No.")]
        public long DeviceImeiNoID { get; set; }

        [Required]
        [DisplayName("Meter S.No.")]
        public long MeterSerialNoID { get; set; }

        [Required]
        [DisplayName("Model Year")]
        public int ModelYear { get; set; }

        //[Required]
        //public string PlateNo { get; set; }

        [Required]
        [DisplayName("Updated By")]
        public string UpdateDetailsUpdatedBy { get; set; }

        [Required]
        [DisplayName("Updated Time")]
        public DateTime UpdateDetailsUpdatedTime { get; set; }

        [Required]
        [DisplayName("Chasis No.")]
        public string VehicleChaseNo { get; set; }

        [Required]
        [DisplayName("Engine No.")]
        public string VehicleEngineNo { get; set; }

        [Required]
        [DisplayName("Make")]
        public int VehicleMakeID { get; set; }
        public VehicleMake VehicleMake { get; set; }

        [Required]
        [DisplayName("Model")]
        public int VehicleModelID { get; set; }
        public VehicleModel VehicleModel { get; set; }

        [Required]
        [DisplayName("Vehicle No.")]
        public string VehicleNo { get; set; }

        [Required]
        [DisplayName("Vehicle Type")]
        public int VehicleTypeID { get; set; }
        public VehicleType VehicleType { get; set; }

        [Required]
        [DisplayName("Owner")]
        public int OwnerID { get; set; }
        public Owner Owner { get; set; }

        //[Required]
        //public string VehicleNature { get; set; }

        //[Required]
        //public int MaxTankCapacity { get; set; }

        //[Required]
        //public int MaxVolt { get; set; }

        //[Required]
        //public string VehicleContactNo { get; set; }

        public static object GetVehicles()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from Vehicle in DB.Vehicles
                        select new
                        {
                            VehicleID = Vehicle.VehicleID,
                            ActiveStatus = Vehicle.ActiveStatus,
                            //ColorID = Vehicle.VehicleColor,
                            DeviceImeiNoID = Vehicle.DeviceImeiNoID,
                            MeterSerialNoID = Vehicle.MeterSerialNoID,
                            ModelYear = Vehicle.ModelYear,
                            //PlateNo = Vehicle.PlateNo,
                            UpdateDetailsUpdatedBy = Vehicle.UpdateDetailsUpdatedBy,
                            UpdateDetailsUpdatedTime = Vehicle.UpdateDetailsUpdatedTime,
                            VehicelChaseNo = Vehicle.VehicleChaseNo,
                            VehicleEngineNo = Vehicle.VehicleEngineNo,
                            VehicleMakeID = Vehicle.VehicleMakeID,
                            VehicleModelID = Vehicle.VehicleModelID,
                            VehicleNo = Vehicle.VehicleNo,
                            VehicleTypeID = Vehicle.VehicleTypeID,
                            OwnerID = Vehicle.OwnerID,
                            //VehicleNature = Vehicle.VehicleNature,
                            //MaxTankCapacity = Vehicle.MaxTankCapacity,
                            //MaxVolt = Vehicle.MaxVolt,
                            //VehicleContactNo = Vehicle.VehicleContactNo,
                        };

            return query.ToList();
        }
    }
}