﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class VehicleType
    {
        public int VehicleTypeID { get; set; }
        [Required]
        [Display(Name = "Vehicle Type")]
        public string Descr { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Created Time")]
        public DateTime CreatedTime { get; set; }
        public string UpdateDetailsUpdatedBy { get; set; }
        public DateTime UpdateDetailsUpdatedTime { get; set; }

        public static object GetVehicleTypes()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from VehicleType in DB.VehicleTypes
                        select new
                        {
                            VehicleTypeID = VehicleType.VehicleTypeID,
                            Descr  = VehicleType.Descr
                        };
            return query.ToList();
        }
    }
}