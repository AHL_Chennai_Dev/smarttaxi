﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class CustomerType
    {
        public long CustomerTypeID { get; set; }

        [Required]
        [Display(Name="Customer Type")]
        public string Descr { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Created Time")]
        public DateTime CreatedTime { get; set; }

        public string UpdateDetailsUpdatedBy { get; set; }
        public DateTime UpdateDetailsUpdatedTime { get; set; }

        public static object GetCustomerTypes()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from CustomerType in DB.CustomerTypes
                        select new
                        {
                            CustomerTypeID = CustomerType.CustomerTypeID,
                            Descr = CustomerType.Descr
                        };

            return query.ToList();
        }
    }
}