﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class Customer
    {
        public long CustomerID { get; set; }

        //public string AlternativeNo { get; set; }

        [Required]
        [Display(Name = "Caller Id")]
        public string CallerID { get; set; }

        //[Display(Name = "Corporate Name")]
        //public string CorporateName { get; set; }

        [Required]
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Customer Type")]
        public int CustomerTypeID { get; set; }
        public CustomerType CustomerType { get; set; }

        //[Display(Name = "Partner")]
        //public long PartnerID { get; set; }

        //[Display(Name = "Partner Member Contact No")]
        //public string PartnerMemberCardNo { get; set; }

        //[Display(Name = "Pickup Main Area")]
        //public int PickupMainAreaID { get; set; }
        //public MainArea PickupMainArea { get; set; }

        //[Display(Name = "Pickup Sub Area")]
        //public int PickupSubAreaID { get; set; }
        //public SubArea PickupSubArea { get; set; }

        //[Display(Name = "Address")]
        //public string PickupAddress { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Created Time")]
        public DateTime CreatedTime { get; set; }

        [Display(Name = "Updated By")]
        public string UpdateDetailsUpdatedBy { get; set; }

        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Updated Time")]
        public DateTime UpdateDetailsUpdatedTime { get; set; }

        public static object GetCustomer()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from Customer in DB.Customers
                        select new
                        {
                            //AlternativeNo = Customer.AlternativeNo,
                            //CorporateName = Customer.CorporateName,
                            //PartnerID = Customer.PartnerID,
                            //PartnerMemberCardNo = Customer.PartnerMemberCardNo,
                            //PickupAddress = Customer.PickupAddress,
                            //PickupMainAreaID = Customer.PickupMainAreaID,
                            //PickupSubAreaID = Customer.PickupSubAreaID,
                            CustomerID = Customer.CustomerID,
                            CallerID = Customer.CallerID,
                            CustomerName = Customer.CustomerName
                            //CustomerTypeID = Customer.CustomerTypeID,
                            //CreatedBy = Customer.CreatedBy,
                            //CreatedTime = Customer.CreatedTime,
                            //UpdateDetailsUpdatedBy = Customer.UpdateDetailsUpdatedBy,
                            //UpdateDetailsUpdatedTime = Customer.UpdateDetailsUpdatedTime
                        };

            return query.ToList();
        }
    }
}