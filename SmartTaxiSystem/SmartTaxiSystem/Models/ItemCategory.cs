﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class ItemCategory
    {
        public long ItemCategoryID { get; set; }

        [Required]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        public string IsActive { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? TimeStamp { get; set; }

        public static object GetItemCategory()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from ItemCategory in DB.ItemCategories
                        select new
                        {
                            CategoryID = ItemCategory.ItemCategoryID,
                            CategoryName = ItemCategory.CategoryName,
                            IsActive = ItemCategory.IsActive
                            //,TimeStamp = ItemCategory.Time_Stamp
                        };

            return query.ToList();
        }
    }
}