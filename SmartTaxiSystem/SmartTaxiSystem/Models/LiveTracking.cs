﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SmartTaxiSystem.Enums;

namespace SmartTaxiSystem.Models
{
    public class LiveTracking
    {
        public long LiveTrackingID { get; set; }

        [Display(Name = "Vehicle No")]
        public string VehicleNo { get; set; }


        [Display(Name = "Date Time")]
        public DateTime DateTime { get; set; }


        [Display(Name = "Speed")]
        public double Speed { get; set; }

        public VehicleStatus Status { get; set; }

        [Display(Name = "Driver Id")]
        public string DriverId { get; set; }

        [Display(Name = "Latitude")]
        public double Latitude { get; set; }


        [Display(Name = "Longitude")]
        public double Longitude { get; set; }

        public LoginStatus LoggedInStatus { get; set; }


        [Display(Name = "Address")]
        public string Address { get; set; }
      
    }
}