﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartTaxiSystem.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartTaxiSystem.Models
{
    public class BookingPickupDrop
    {
        public int BookingPickupDropID { get; set; }
        public int BookingID { get; set; }
        public Booking Booking { get; set; }
        public MainArea PickupMainArea { get; set; }
        public SubArea PickupSubArea { get; set; }
        public string PickupAddress { get; set; }
        public MainArea DropMainArea { get; set; }
        public SubArea DropSubArea { get; set; }
        public string DropAddress { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Created Time")]
        public DateTime CreatedTime { get; set; }
        [Display(Name = "Updated By")]
        public string UpdateDetailsUpdatedBy { get; set; }
        [Display(Name = "Updated Time")]
        public DateTime UpdateDetailsUdpatedTime { get; set; }

    }
}