﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class EmployeeDepartment
    {
        public int EmployeeDepartmentID { get; set; }
        public string DepartmentName { get; set; }

        public static object GetEmployeeDepartments()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from EmployeeDepartment in DB.EmployeeDepartments
                        select new
                        {
                            EmployeeDepartmentID = EmployeeDepartment.EmployeeDepartmentID,
                            DepartmentName = EmployeeDepartment.DepartmentName
                        };

            return query.ToList();
        }
    }
}