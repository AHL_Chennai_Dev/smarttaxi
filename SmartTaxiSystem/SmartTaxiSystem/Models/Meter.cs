﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class Meter
    {
        public int MeterID { get; set; }
        [Required]
        public string CountryOfOrigin { get; set; }
        [Required]
        public string DeviceImeiNo { get; set; }
        [Required]
        public string ModelName { get; set; }
        [Required]
        public string ModelType { get; set; }
        [Required]
        public DateTime PurchaseDate { get; set; }
        [Required]
        public long SimCardNoID { get; set; }
        [Required]
        public string UpdateDetailsUpdatedBy { get; set; }
        [Required]
        public DateTime UpdateDetailsUpdatedTime { get; set; }
        [Required]
        public long VehicleID { get; set; }
        [Required]
        public string DeviceSerialNo { get; set; }

        public static object GetMeters()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from Meter in DB.Meters
                        select new
                        {
                            MeterID = Meter.MeterID,
                            CountryOfOrigin = Meter.CountryOfOrigin,
                            DeviceImeiNo = Meter.DeviceImeiNo,
                            ModelName = Meter.ModelName,
                            ModelType = Meter.ModelType,
                            PurchaseDate = Meter.PurchaseDate,
                            SimCardNoID = Meter.SimCardNoID,
                            UpdateDetailsUpdatedBy = Meter.UpdateDetailsUpdatedBy,
                            UpdateDetailsUpdatedTime = Meter.UpdateDetailsUpdatedTime,
                            VehicleId = Meter.VehicleID,
                            DeviceSerialNo = Meter.DeviceSerialNo,
                        };

            return query.ToList();
        }
    }
}