﻿using SmartTaxiSystem.Enums;
//using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class Employee
    {
        public long EmployeeID { get; set; }

        [Required]
        [Display(Name="Active Status")]
        public ActiveStatus EmployeeActiveStatus { get; set; }

        [Display(Name="Address")]
        public string Address { get; set; }

        [Display(Name="Bank Accout No.")]
        public string BankAccountNo { get; set; }

        [Display(Name="Bank Name")]
        public string BankName { get; set; }

        [Required]
        [Display(Name="Contact No")]
        public string ContactNo { get; set; }

        [Display(Name="Date of Birth")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name="Employee Id")]
        public string EmployeeNo { get; set; }

        [Display(Name="Family Contact Person")]
        public string FamilyContactPersonName { get; set; }

        [Display(Name="Family Contact No.")]
        public string FamilyContactPersonNo { get; set; }

        [Required]
        [Display(Name="First Name")]
        public string FirstName { get; set; }

        [Display(Name="Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name="Gender")]
        public Gender EmployeeGender { get; set; }

        [MaxLength(50)]
        [Display(Name="ID Card")]
        public byte[] IDCardDoc { get; set; }
        
        [MaxLength(50)]
        [Display(Name="License")]
        public byte[] LicenseDoc { get; set; }

        [Display(Name="License Exp Date")]
        public DateTime? LicenseExpiryDate { get; set; }

        [Display(Name="License No")]
        public string LicenseNo { get; set; }

        [Required]
        [Display(Name="Login ID")]
        public string LoginId { get; set; }

        public string LoginSessionId { get; set; }

        public LoginStatus EmployeeLoginStatus { get; set; }

        //public DateTime? LoginTime { get; set; }

        [Required]
        [Display(Name="Login Type")]
        public LoginType EmployeeLoginType { get; set; }

        [Display(Name="Marital Status")]
        public MaritalStatus EmployeeMaritalStatus { get; set; }

        [Required]
        [Display(Name="Nationality")]
        public Nationality EmployeeNationality { get; set; }

        [MaxLength(50)]
        [Display(Name = "Passport")]
        public byte[] PassportDoc { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Photo")]
        public long? PhotoDoc { get; set; }

        ////[Required]
        //[Display(Name = "Updated By")]
        //public string UpdateDetailsUpdatedBy { get; set; }

        //[Display(Name = "Updated Time")]
        //public DateTime UpdateDetailsUpdatedTime { get; set; }

        [Display(Name = "Owner")]
        //[ForeignKey("Owner")]
        public int OwnerID { get; set; }
        public Owner Owner { get; set; }

        [Display(Name = "Role")]
        //[ForeignKey("Role")]
        public int RoleID { get; set; }
        public Role Role { get; set; }

        [Display(Name = "Department")]
        //[ForeignKey("Role")]
        public int EmployeeDepartmentID { get; set; }
        public EmployeeDepartment EmployeeDepartment { get; set; }

    }

}