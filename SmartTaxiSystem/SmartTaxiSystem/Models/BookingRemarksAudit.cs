﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class BookingRemarksAudit
    {
        public int BookingRemarksAuditID { get; set; }
        public int BookingID { get; set; }
        public Booking Booking { get; set; }
        public string Remarks { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Created Time")]
        public DateTime CreatedTime { get; set; }
        [Display(Name = "Updated By")]
        public string UpdateDetailsUpdatedBy { get; set; }
        [Display(Name = "Updated Time")]
        public DateTime UpdateDetailsUdpatedTime { get; set; }
    }
}