﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartTaxiSystem.Enums;
using System.ComponentModel.DataAnnotations;

namespace SmartTaxiSystem.Models
{
    public class BookingAssign
    {
        public int BookingAssignID { get; set; }
        [Display(Name="Booking No.")]
        public int BookingID { get; set; }
        public Booking Booking { get; set; }
        [Display(Name = "Booking Status")]
        public BookingStatus Status { get; set; }
        [Display(Name = "Vehicle No.")]
        public int VehicleID { get; set; }
        public Vehicle Vehicle { get; set; }
        [Display(Name = "Driver ID")]
        public int DriverID { get; set; }
        public Employee Employee { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Created Time")]
        public DateTime CreatedTime { get; set; }
        [Display(Name = "Updated By")]
        public string UpdateDetailsUpdatedBy { get; set; }
        [Display(Name = "Updated Time")]
        public DateTime UpdateDetailsUpdatedTime { get; set; }
    }
}