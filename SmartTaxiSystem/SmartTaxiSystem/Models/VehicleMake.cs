﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class VehicleMake
    {
        public int VehicleMakeID { get; set; }

        [Display(Name="Vehicle Make")]
        public string Descr { get; set; }

        public static object GetVehicleMakes()
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            var query = from VehicleMake in DB.VehicleMakes
                        select new
                        {
                            VehicleMakeID = VehicleMake.VehicleMakeID,
                            Descr = VehicleMake.Descr
                        };
            return query.ToList();
        }
    }
}