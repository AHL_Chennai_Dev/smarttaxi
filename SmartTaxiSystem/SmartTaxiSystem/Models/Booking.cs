﻿using SmartTaxiSystem.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Models
{
    public class Booking
    {
        public int BookingID { get; set; }
        
        [Required]
        [Display(Name="Customer Name")]
        public int CustomerID { get; set; }
        public Customer Customer { get; set; }

        public VehicleType VehicleType { get; set; }
        //public VehicleType VehicleType { get; set; }

        [Display(Name = "Pickup Time")]
        public DateTime PickupDateTime { get; set; }
        [Display(Name = "No. of Taxis")]
        public int NoOfTaxi { get; set; }
        [Display(Name = "Pickup Instruction")]
        public string PickupInstruction { get; set; }
        [Display(Name = "Drop Instruction")]
        public string DropInstruction { get; set; }
        [Display(Name = "Special Services")]
        public string SpecialServices { get; set; }
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
        [Display(Name = "Booking Status")]
        public BookingStatus Status { get; set; }
        [Display(Name = "Status Updated Time")]
        public DateTime StatusUpdatedTime { get; set; }
        [Display(Name = "Booking Type")]
        public BookingType BookingType { get; set; }
        [Display(Name = "Dispatched By")]
        public string DispatchedBy { get; set; }
        [Display(Name = "Dispatched Time")]
        public DateTime DispatchedTime { get; set; }
        [Display(Name = "Dispatch Type")]
        public DispatchType DispatchType { get; set; }
        [Display(Name = "Acepted Vehicle")]
        public int AcceptedVehicle { get; set; }
        public Vehicle Vehicle { get; set; }
        [Display(Name = "Accepted Driver")]
        public int AcceptedDriver { get; set; }
        public Employee Employee { get; set; }
        [Display(Name = "Accepted Time")]
        public DateTime AcceptedTime { get; set; }
        [Display(Name = "SMS Booking Count")]
        public int SmsBookingCount { get; set; }
        [Display(Name = "SMS Vehicle Count")]
        public int SmsVehicleCount { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Created Time")]
        public DateTime CreatedTime { get; set; }
        [Display(Name = "Updated By")]
        public string UpdateDetailsUpdatedBy { get; set; }
        [Display(Name = "Updated Time")]
        public DateTime UpdateDetailsUdpatedTime { get; set; }
    }
}