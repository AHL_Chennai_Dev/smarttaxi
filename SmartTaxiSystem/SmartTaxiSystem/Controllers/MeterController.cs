﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;

namespace SmartTaxiSystem.Controllers
{
    public class MeterController : Controller
    {
        //
        // GET: /Meter/
        public ActionResult List()
        {
            return View();
        }

        //SmartTaxiSystem.Context.SmartTaxiContext db = new SmartTaxiSystem.Context.SmartTaxiContext();

        //[ValidateInput(false)]
        //public ActionResult MeterGridView2Partial()
        //{
        //    var model = db.Meters;
        //    return PartialView("_MeterGridView2Partial", model.ToList());
        //}

        SmartTaxiSystem.Context.SmartTaxiContext db = new SmartTaxiSystem.Context.SmartTaxiContext();

        [ValidateInput(false)]
        public ActionResult MeterGridViewPartial()
        {
            var model = db.Meters;
            return PartialView("_MeterGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult MeterGridViewPartialAddNew(SmartTaxiSystem.Models.Meter item)
        {
            var model = db.Meters;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_MeterGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult MeterGridViewPartialUpdate(SmartTaxiSystem.Models.Meter item)
        {
            var model = db.Meters;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.MeterID == item.MeterID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_RoleGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult MeterGridViewPartialDelete(System.Int32 MeterID)
        {
            var model = db.Meters;
            if (MeterID >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.MeterID == MeterID);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_MeterGridViewPartial", model.ToList());
        }
	}
}