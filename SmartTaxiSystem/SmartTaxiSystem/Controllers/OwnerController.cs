﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;

namespace SmartTaxiSystem.Controllers
{
    public class OwnerController : Controller
    {
        //
        // GET: /Owner/
        public ActionResult List()
        {
            return View();
        }

        SmartTaxiSystem.Context.SmartTaxiContext db = new SmartTaxiSystem.Context.SmartTaxiContext();

        [ValidateInput(false)]
        public ActionResult OwnerGridViewPartial()
        {
            var model = db.Owners;
            return PartialView("_OwnerGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult OwnerGridViewPartialAddNew(SmartTaxiSystem.Models.Owner item)
        {
            var model = db.Owners;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_OwnerGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult OwnerGridViewPartialUpdate(SmartTaxiSystem.Models.Owner item)
        {
            var model = db.Owners;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.OwnerID == item.OwnerID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_OwnerGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult OwnerGridViewPartialDelete(System.Int32 OwnerID)
        {
            var model = db.Owners;
            if (OwnerID >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.OwnerID == OwnerID);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_OwnerGridViewPartial", model.ToList());
        }
	}
}