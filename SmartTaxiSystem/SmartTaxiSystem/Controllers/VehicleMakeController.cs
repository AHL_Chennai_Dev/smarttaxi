﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using SmartTaxiSystem.Context;
using SmartTaxiSystem.Models;

namespace SmartTaxiSystem.Controllers
{
    public class VehicleMakeController : Controller
    {
        // GET: VehicleMake
        public ActionResult List()
        {
            return View();
        }

        SmartTaxiContext DB = new SmartTaxiContext();

        [ValidateInput(false)]
        public ActionResult VehicleMakeGridViewPartial()
        {
            var model = DB.VehicleMakes;
            return PartialView("_VehicleMakeGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult VehicleMakeGridViewPartialAddNew(VehicleMake item)
        {
            var model = DB.VehicleMakes;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_VehicleMakeGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult VehicleMakeGridViewPartialUpdate(VehicleMake item)
        {
            var model = DB.VehicleMakes;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.VehicleMakeID == item.VehicleMakeID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        DB.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_VehicleMakeGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult VehicleMakeGridViewPartialDelete(System.Int64 id)
        {
            var model = DB.VehicleMakes;
            if (id >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.VehicleMakeID == id);
                    if (item != null)
                        model.Remove(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_VehicleMakeGridViewPartial", model.ToList());
        }
    }
}