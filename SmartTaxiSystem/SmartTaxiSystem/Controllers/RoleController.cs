﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;

namespace SmartTaxiSystem.Controllers
{
    public class RoleController : Controller
    {
        //
        // GET: /Role/
        public ActionResult List()
        {
            return View();
        }

        //SmartTaxiSystem.Context.SmartTaxiContext db = new SmartTaxiSystem.Context.SmartTaxiContext();

        //[ValidateInput(false)]
        //public ActionResult RoleGridViewPartial()
        //{
        //    var model = db.Roles;
        //    return PartialView("_RoleGridViewPartial", model.ToList());
        //}

        SmartTaxiSystem.Context.SmartTaxiContext db = new SmartTaxiSystem.Context.SmartTaxiContext();

        [ValidateInput(false)]
        public ActionResult RoleGridViewPartial()
        {
            var model = db.Roles;
            return PartialView("_RoleGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult RoleGridViewPartialAddNew(SmartTaxiSystem.Models.Role item)
        {
            var model = db.Roles;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_RoleGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult RoleGridViewPartialUpdate(SmartTaxiSystem.Models.Role item)
        {
            var model = db.Roles;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.RoleID == item.RoleID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_RoleGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult RoleGridViewPartialDelete(System.Int32 RoleID)
        {
            var model = db.Roles;
            if (RoleID >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.RoleID == RoleID);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_RoleGridViewPartial", model.ToList());
        }
	}
}