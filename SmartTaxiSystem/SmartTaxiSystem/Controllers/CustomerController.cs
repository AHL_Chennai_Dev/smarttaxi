﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using SmartTaxiSystem.Context;

namespace SmartTaxiSystem.Controllers
{
    public class CustomerController : Controller
    {
        SmartTaxiContext DB = new SmartTaxiContext();

        // GET: Customer
        public ActionResult List()
        {
            return View();
        }

        [ValidateInput(false)]
        public ActionResult CustomerGridViewPartial()
        {
            var model = DB.Customers;
            return PartialView("_CustomerGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CustomerGridViewPartialAddNew(SmartTaxiSystem.Models.Customer item)
        {
            var model = DB.Customers;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_CustomerGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult CustomerGridViewPartialUpdate(SmartTaxiSystem.Models.Customer item)
        {
            var model = DB.Customers;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.CustomerID == item.CustomerID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        DB.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_CustomerGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult CustomerGridViewPartialDelete(System.Int64 CustomerID)
        {
            var model = DB.Customers;
            if (CustomerID >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.CustomerID == CustomerID);
                    if (item != null)
                        model.Remove(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_CustomerGridViewPartial", model.ToList());
        }
    }
}