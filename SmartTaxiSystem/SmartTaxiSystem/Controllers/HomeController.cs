using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartTaxiSystem.Models;
using System.Collections;
using DevExpress.Web.Mvc;
using SmartTaxiSystem.Context;

namespace SmartTaxiSystem.Controllers
{
    public class HomeController : Controller
    {
        SmartTaxiContext DB = new SmartTaxiContext();

        public ActionResult Index()
        {
            // DXCOMMENT: Pass a data model for GridView
            if (Session["LoggedInUser"] == null)
            {
                return RedirectToAction("Login", "Account");
            }

            return View();
        }
        
        //public ActionResult GridViewPartialView() 
        //{
        //    // DXCOMMENT: Pass a data model for GridView in the PartialView method's second parameter
        //    return PartialView("GridViewPartialView");
        //}

        //SmartTaxiContext DB = new SmartTaxiContext();

        //[ValidateInput(false)]
        //public ActionResult VehicleMakeGridViewPartial()
        //{
        //    var model = DB.VehicleMakes;
        //    return PartialView("_VehicleMakeGridViewPartial", model.ToList());
        //}

        //[HttpPost, ValidateInput(false)]
        //public ActionResult VehicleMakeGridViewPartialAddNew(VehicleMake item)
        //{
        //    var model = DB.VehicleMakes;
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            model.Add(item);
        //            DB.SaveChanges();
        //        }
        //        catch (Exception e)
        //        {
        //            ViewData["EditError"] = e.Message;
        //        }
        //    }
        //    else
        //        ViewData["EditError"] = "Please, correct all errors.";
        //    return PartialView("_VehicleMakeGridViewPartial", model.ToList());
        //}
        //[HttpPost, ValidateInput(false)]
        //public ActionResult VehicleMakeGridViewPartialUpdate(VehicleMake item)
        //{
        //    var model = DB.VehicleMakes;
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            var modelItem = model.FirstOrDefault(it => it.VehicleMakeID == item.VehicleMakeID);
        //            if (modelItem != null)
        //            {
        //                this.UpdateModel(modelItem);
        //                DB.SaveChanges();
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            ViewData["EditError"] = e.Message;
        //        }
        //    }
        //    else
        //        ViewData["EditError"] = "Please, correct all errors.";
        //    return PartialView("_VehicleMakeGridViewPartial", model.ToList());
        //}
        //[HttpPost, ValidateInput(false)]
        //public ActionResult VehicleMakeGridViewPartialDelete(System.Int64 id)
        //{
        //    var model = DB.VehicleMakes;
        //    if (id >= 0)
        //    {
        //        try
        //        {
        //            var item = model.FirstOrDefault(it => it.VehicleMakeID == id);
        //            if (item != null)
        //                model.Remove(item);
        //            DB.SaveChanges();
        //        }
        //        catch (Exception e)
        //        {
        //            ViewData["EditError"] = e.Message;
        //        }
        //    }
        //    return PartialView("_VehicleMakeGridViewPartial", model.ToList());
        //}

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            if (Session["loggedInUser"] != null)
            {
                System.Diagnostics.Debug.WriteLine("before loggedInUser setted as null : " + Session["loggedInUser"]);
                Session["loggedInUser"] = null;
                Session.Clear();
                Session.Abandon();
                System.Diagnostics.Debug.WriteLine("loggedInUser setted as null : " + Session["loggedInUser"]);
            }
            return RedirectToAction("Login", "Account");
        }
    }
}

public enum HeaderViewRenderMode { Full, Title }