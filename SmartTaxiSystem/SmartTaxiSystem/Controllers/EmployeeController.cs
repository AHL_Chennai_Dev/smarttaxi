﻿using SmartTaxiSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using DevExpress.Web;
using SmartTaxiSystem.Context;

namespace SmartTaxiSystem.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }

        // GET: Employee List
        public ActionResult List()
        {
            return View();
        }

        // GET: /Employee/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Employee employee)
        {
            SmartTaxiContext DB = new SmartTaxiContext();

            //if (ModelState.IsValid)
            //{
                //if(WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe.Value)) {
                //    return Redirect(returnUrl ?? "/");
                //} 
                var v = DB.Employees.Where(a => a.LoginId.Equals(employee.LoginId) && a.Password.Equals(employee.Password)).FirstOrDefault();
                if (v != null)
                {
                    Session["loggedInUser"] = new Employee() { LoginId = employee.LoginId, Password = employee.Password, FirstName = employee.FirstName };
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
            //}

            // If we got this far, something failed, redisplay form
            return View(employee);
        }

        SmartTaxiContext DB = new SmartTaxiContext();

        [ValidateInput(false)]
        public ActionResult EmployeeGridViewPartial()
        {
            var model = DB.Employees;
            return PartialView("_EmployeeGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EmployeeGridViewPartialAddNew(SmartTaxiSystem.Models.Employee item)
        {
            var model = DB.Employees;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_EmployeeGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EmployeeGridViewPartialUpdate(SmartTaxiSystem.Models.Employee item)
        {
            var model = DB.Employees;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.EmployeeID == item.EmployeeID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        DB.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_EmployeeGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult EmployeeGridViewPartialDelete(System.Int64 id)
        {
            var model = DB.Employees;
            if (id >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.EmployeeID == id);
                    if (item != null)
                        model.Remove(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_EmployeeGridViewPartial", model.ToList());
        }

        public ActionResult BinaryImageColumnPhotoUpdate()
        {
            return BinaryImageEditExtension.GetCallbackResult();
        }
    }
}