﻿using SmartTaxiSystem.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartTaxiSystem.Controllers
{
    public class ItemCategoryController : Controller
    {
        // GET: ItemCategory
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            return View();
        }

        SmartTaxiContext DB = new SmartTaxiContext();

        [ValidateInput(false)]
        public ActionResult ItemCategoryGridViewPartial()
        {
            var model = DB.ItemCategories;
            return PartialView("_ItemCategoryGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult ItemCategoryGridViewPartialAddNew(SmartTaxiSystem.Models.ItemCategory item)
        {
            var model = DB.ItemCategories;
            if (ModelState.IsValid)
            {
                try
                {
                    if (item.IsActive == null)
                    {
                        item.IsActive = "Y";
                    }
                    else if (item.TimeStamp == null)
                    {
                        item.TimeStamp = Convert.ToDateTime(DateTime.Now);
                    }
                    model.Add(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_ItemCategoryGridViewPartial", model.ToList());
        }
        
        [HttpPost, ValidateInput(false)]
        public ActionResult ItemCategoryGridViewPartialUpdate(SmartTaxiSystem.Models.ItemCategory item)
        {
            var model = DB.ItemCategories;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.ItemCategoryID == item.ItemCategoryID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        DB.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_ItemCategoryGridViewPartial", model.ToList());
        }
        
        [HttpPost, ValidateInput(false)]
        public ActionResult ItemCategoryGridViewPartialDelete(System.Int64 id)
        {
            var model = DB.ItemCategories;
            if (id >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.ItemCategoryID == id);
                    if (item != null)
                        model.Remove(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_ ItemCategoryGridViewPartial", model.ToList());
        }
    }
}