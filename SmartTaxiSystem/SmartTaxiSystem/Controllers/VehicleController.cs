﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;

namespace SmartTaxiSystem.Controllers
{
    public class VehicleController : Controller
    {
        //
        // GET: /Vehicle/
        public ActionResult List()
        {
            return View();
        }

        SmartTaxiSystem.Context.SmartTaxiContext db = new SmartTaxiSystem.Context.SmartTaxiContext();

        [ValidateInput(false)]
        public ActionResult VehicleGridViewPartial()
        {
            var model = db.Vehicles;
            return PartialView("_VehicleGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult VehicleGridViewPartialAddNew(SmartTaxiSystem.Models.Vehicle item)
        {
            var model = db.Vehicles;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_VehicleGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult VehicleGridViewPartialUpdate(SmartTaxiSystem.Models.Vehicle item)
        {
            var model = db.Vehicles;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.VehicleID == item.VehicleID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_VehicleGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult VehicleGridViewPartialDelete(System.Int32 VehicleID)
        {
            var model = db.Vehicles;
            if (VehicleID >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.VehicleID == VehicleID);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_VehicleGridViewPartial", model.ToList());
        }
	}
}