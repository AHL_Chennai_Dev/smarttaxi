﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using SmartTaxiSystem.Context;

namespace SmartTaxiSystem.Controllers
{
    public class CustomerTypeController : Controller
    {
        SmartTaxiContext DB = new SmartTaxiContext();
        // GET: CustomerType
        public ActionResult List()
        {
            return View();
        }

        [ValidateInput(false)]
        public ActionResult CustomerTypeGridViewPartial()
        {
            var model = DB.CustomerTypes;
            return PartialView("_CustomerTypeGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CustomerTypeGridViewPartialAddNew(SmartTaxiSystem.Models.CustomerType item)
        {
            var model = DB.CustomerTypes;
            //long CustomerId = 0;
            string CustomerName = null;
            if (Session["loggedInUser"] != null)
            {
                //CustomerId = Convert.ToInt64(Session["logginUserID"]);
                CustomerName = Session["loggedInUser"].ToString();
            }


            if (ModelState.IsValid)
            {
                try
                {
                    item.UpdateDetailsUpdatedBy = CustomerName;
                    item.UpdateDetailsUpdatedTime = DateTime.Now;
                    model.Add(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_CustomerTypeGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult CustomerTypeGridViewPartialUpdate(SmartTaxiSystem.Models.CustomerType item)
        {
            var model = DB.CustomerTypes;
            string CustomerName = null;

            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.CustomerTypeID == item.CustomerTypeID);
                    if (modelItem != null)
                    {
                        if (Session["loggedInUser"] != null)
                        {
                            CustomerName = Session["loggedInUser"].ToString();
                        }
                        modelItem.UpdateDetailsUpdatedBy = CustomerName;
                        modelItem.UpdateDetailsUpdatedTime = DateTime.Now;
                        this.UpdateModel(modelItem);
                        DB.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_CustomerTypeGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult CustomerTypeGridViewPartialDelete(System.Int64 CustomerTypeID)
        {
            var model = DB.CustomerTypes;
            if (CustomerTypeID >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.CustomerTypeID == CustomerTypeID);
                    if (item != null)
                        model.Remove(item);
                    DB.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_CustomerTypeGridViewPartial", model.ToList());
        }
    }
}