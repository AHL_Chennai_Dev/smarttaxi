﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;

namespace SmartTaxiSystem.Controllers
{
    public class VehicleModelController : Controller
    {
        //
        // GET: /VehicleModel/
        public ActionResult List()
        {
            return View();
        }

        //SmartTaxiSystem.Context.SmartTaxiContext db = new SmartTaxiSystem.Context.SmartTaxiContext();

        //[ValidateInput(false)]
        //public ActionResult VehicleModelGridViewPartial()
        //{
        //    var model = db.VehicleModels;
        //    return PartialView("_VehicleModelGridViewPartial", model.ToList());
        //}

        SmartTaxiSystem.Context.SmartTaxiContext db = new SmartTaxiSystem.Context.SmartTaxiContext();

        [ValidateInput(false)]
        public ActionResult VehicleModelGridViewPartial()
        {
            var model = db.VehicleModels;
            return PartialView("_VehicleModelGridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult VehicleModelGridViewPartialAddNew(SmartTaxiSystem.Models.VehicleModel item)
        {
            var model = db.VehicleModels;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_VehicleModelGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult VehicleModelGridViewPartialUpdate(SmartTaxiSystem.Models.VehicleModel item)
        {
            var model = db.VehicleModels;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.VehicleModelID == item.VehicleModelID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_VehicleModelGridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult RoleGridViewPartialDelete(System.Int32 VehicleModelID)
        {
            var model = db.VehicleModels;
            if (VehicleModelID >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.VehicleModelID == VehicleModelID);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_VehicleModelGridViewPartial", model.ToList());
        }
	}
}