﻿using SmartTaxiSystem.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SmartTaxiSystem.Context
{
    public class SmartTaxiContext : DbContext
    {

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        // Employee Masters
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<EmployeeDepartment> EmployeeDepartments { get; set; }

        // Vehicle Masters
        public DbSet<VehicleMake> VehicleMakes { get; set; }
        public DbSet<VehicleModel> VehicleModels { get; set; }
        public DbSet<VehicleType> VehicleTypes { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Meter> Meters { get; set; }

        // Customer Masters
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerType> CustomerTypes { get; set; }

        // Tracking
        public DbSet<LiveTracking> LiveTrackings { get; set; }

        // Booking
        public DbSet<MainArea> MainAreas { get; set; }
        public DbSet<SubArea> SubAreas { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<BookingAssign> BookingAssigns { get; set; }
        public DbSet<BookingBid> BookingBids { get; set; }
        public DbSet<BookingCancel> BookingCancels { get; set; }
        public DbSet<BookingPickupDrop> BookingPickupDrops { get; set; }
        public DbSet<BookingRemarksAudit> BookingRemarksAudits { get; set; }

        // Lost & Found
        public DbSet<ItemCategory> ItemCategories { get; set; }
    }
}