﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Enums
{
    public enum VehicleStatus
    {
        Free = 1,
        Hired = 2,
        Busy = 3,
        OnCall = 4,
    };
}