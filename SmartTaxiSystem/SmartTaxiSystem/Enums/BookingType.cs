﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Enums
{
    public enum BookingType
    {
        Web = 1,
        Mobile = 2
    };
}