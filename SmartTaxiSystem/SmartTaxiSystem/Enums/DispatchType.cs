﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Enums
{
    public enum DispatchType
    {
        Automatic = 1,
        Manaul = 2,
        SemiAutomatic = 3
    };
}