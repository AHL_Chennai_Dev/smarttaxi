﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Enums
{
    public enum VehicleActiveStatus
    {
        [DisplayName("On Road")]
        OnRoad = 1,
        OffRoad = 2,
        Cancelled = 3,
        Replaced = 4
    }
}