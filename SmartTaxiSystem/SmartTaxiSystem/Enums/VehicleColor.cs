﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Enums
{
    public enum VehicleColor
    {
        Black = 1,
        White = 2
    }
}