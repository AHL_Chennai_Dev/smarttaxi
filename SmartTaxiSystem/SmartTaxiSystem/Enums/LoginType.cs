﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTaxiSystem.Enums
{
    public enum LoginType
    {
        Staff = 1, 
        Driver = 2
    }
}