﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTaxiSystem.Enums
{
    public enum Nationality
    {
        India = 1,
        Nepal = 2,
        Bangladesh = 3,
        [Display(Name = "Sri Lanka")]
        SriLanka = 4
    }
}