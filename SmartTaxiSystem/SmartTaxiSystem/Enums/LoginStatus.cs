﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTaxiSystem.Enums
{
    public enum LoginStatus
    {
        [Display(Name = "Logged In")]
        LoggedIn = 1,
        [Display(Name = "Not Logged In")]
        NotLoggedIn = 2
    }
}