﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartTaxiSystem.Enums
{
    public enum ActiveStatus
    {
        Active = 1, 
        Suspended = 2, 
        Blocked = 3, 
        Inactive = 4, 
        Vacation = 5, 
        Exit = 6
    }
}