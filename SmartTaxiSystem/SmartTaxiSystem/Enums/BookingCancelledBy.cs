﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTaxiSystem.Enums
{
    public enum BookingCancelledBy
    {
        Customer = 1,
        Driver = 2,
        CallAgent = 3
    };
}