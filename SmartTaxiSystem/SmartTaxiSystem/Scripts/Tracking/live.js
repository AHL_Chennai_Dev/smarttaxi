﻿$(document).ready(function () {
    console.log("this");
    initialize();
});
function initialize() {
    $.ajax({
        url: "../Tracking", // to get the right path to controller from TableRoutes of Asp.Net MVC
        dataType: "json", //to work with json format
        type: "POST", //to do a post request 
        contentType: 'application/json; charset=utf-8', //define a contentType of your request
        cache: false, //avoid caching results
        data: {}, // here you can pass arguments to your request if you need
        success: function (data) {
            // data is your result from controller
            console.log('jfj');
           
            renderMap(data.vehicles);
            
        },
        error: function (xhr) {
            alert('error'+xhr.responseText);
        }
    });
  //  var liveTrackingVehicles = '@Url.Action("getLiveTrackingVehicles", "Tracking")';
  /*  $.ajax({
        url: '@Url.Action("getLiveTrackingVehicles", "~/Controllers/Tracking")',
        method: 'GET',
        success: function (data) { alert('yes'); }
        
    });
    $.getJSON("../getLiveTrackingVehicles",
    function (data) {
        alert(data);
    }
);*/
   
}


function renderMap(markers) {
    console.log("renderMap");
    var infowindow = new google.maps.InfoWindow()
    var self = this,
				mapOptions = {
				    center: new google.maps.LatLng(25.257920900, 55.32229069),
				    zoom: 15,
				    mapTypeId: google.maps.MapTypeId.ROADMAP
				};

    this.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    var iconUrl = ROOT + 'Content/Images/Icons/taxi-free.png';
    
    $.each(markers, function (i, p) {
        console.log(p.Latitude);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(p.Latitude, p.Longitude),
            icon:iconUrl,
            map: self.map,
            status: p.Status,
            vehicleNo: p.VehicleNo
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent("Vehicle No : " + p.VehicleNo + "<br>"
                        + "Speed : " + p.Speed + "<br>"
                        + "Address : " + p.Address + "<br>"
                        );
                infowindow.open(self.map, marker);
                self.map.panTo(marker.getPosition())
            }
        })(marker, i));
    });


    console.log("end of render");
  

	
			
}

function searchVehicle() {
    var selectedValue = vehicles.GetValue();
    console.log(selectedValue);
    $.ajax({
        url: "../Tracking", // to get the right path to controller from TableRoutes of Asp.Net MVC
        dataType: "json", //to work with json format
        type: "POST", //to do a post request 
        contentType: 'application/json; charset=utf-8', //define a contentType of your request
        cache: false, //avoid caching results
        data: {}, // here you can pass arguments to your request if you need
        success: function (data) {
            // data is your result from controller
            console.log('jfj');

            renderSelectedMap(data.vehicles, selectedValue);

        },
        error: function (xhr) {
            alert('error' + xhr.responseText);
        }
    });
}
function renderSelectedMap(markers, selectedValue) {
     var mlat;
        var mlong;
        var mlat;
        var mlong;
        $.each(markers, function (i, p) {
            var flag = false;
            if (selectedValue == p.VehicleNo) {
                mlat = p.Latitude;
                mlong = p.Longitude;


            }
        });
    console.log("renderMap1");
    var infowindow = new google.maps.InfoWindow()
    var self = this,
				mapOptions = {
				    center: new google.maps.LatLng(mlat, mlong),
				    zoom: 15,
				    mapTypeId: google.maps.MapTypeId.ROADMAP
				};

    this.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    var iconUrl = ROOT + 'Content/Images/Icons/taxi-free.png';

    $.each(markers, function (i, p) {
        var flag = false;
        if (selectedValue == p.VehicleNo) {
            flag = true;
        }
        if (flag) {
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(p.Latitude, p.Longitude),
                icon: iconUrl,
                map: self.map,
                status: p.Status,
                vehicleNo: p.VehicleNo
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent("Vehicle No : " + p.VehicleNo + "<br>"
                            + "Speed : " + p.Speed + "<br>"
                            + "Address : " + p.Address + "<br>"
                            );
                    infowindow.open(self.map, marker);
                    self.map.panTo(marker.getPosition())
                }
            })(marker, i));
        }
    });

    console.log("end of render");




}
