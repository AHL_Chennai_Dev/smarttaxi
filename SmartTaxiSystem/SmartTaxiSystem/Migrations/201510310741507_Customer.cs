namespace SmartTaxiSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Customer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookingAssign",
                c => new
                    {
                        BookingAssignID = c.Int(nullable: false, identity: true),
                        BookingID = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        VehicleID = c.Int(nullable: false),
                        DriverID = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UpdateDetailsUpdatedBy = c.String(),
                        UpdateDetailsUpdatedTime = c.DateTime(nullable: false),
                        Employee_EmployeeID = c.Long(),
                    })
                .PrimaryKey(t => t.BookingAssignID)
                .ForeignKey("dbo.Booking", t => t.BookingID, cascadeDelete: true)
                .ForeignKey("dbo.Employee", t => t.Employee_EmployeeID)
                .ForeignKey("dbo.Vehicle", t => t.VehicleID, cascadeDelete: true)
                .Index(t => t.BookingID)
                .Index(t => t.VehicleID)
                .Index(t => t.Employee_EmployeeID);
            
            CreateTable(
                "dbo.Booking",
                c => new
                    {
                        BookingID = c.Int(nullable: false, identity: true),
                        CustomerID = c.Int(nullable: false),
                        PickupDateTime = c.DateTime(nullable: false),
                        NoOfTaxi = c.Int(nullable: false),
                        PickupInstruction = c.String(),
                        DropInstruction = c.String(),
                        SpecialServices = c.String(),
                        Remarks = c.String(),
                        Status = c.Int(nullable: false),
                        StatusUpdatedTime = c.DateTime(nullable: false),
                        BookingType = c.Int(nullable: false),
                        DispatchedBy = c.String(),
                        DispatchedTime = c.DateTime(nullable: false),
                        DispatchType = c.Int(nullable: false),
                        AcceptedVehicle = c.Int(nullable: false),
                        AcceptedDriver = c.Int(nullable: false),
                        AcceptedTime = c.DateTime(nullable: false),
                        SmsBookingCount = c.Int(nullable: false),
                        SmsVehicleCount = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UpdateDetailsUpdatedBy = c.String(),
                        UpdateDetailsUdpatedTime = c.DateTime(nullable: false),
                        Customer_CustomerID = c.Long(),
                        Employee_EmployeeID = c.Long(),
                        Vehicle_VehicleID = c.Int(),
                        VehicleType_VehicleTypeID = c.Int(),
                    })
                .PrimaryKey(t => t.BookingID)
                .ForeignKey("dbo.Customer", t => t.Customer_CustomerID)
                .ForeignKey("dbo.Employee", t => t.Employee_EmployeeID)
                .ForeignKey("dbo.Vehicle", t => t.Vehicle_VehicleID)
                .ForeignKey("dbo.VehicleType", t => t.VehicleType_VehicleTypeID)
                .Index(t => t.Customer_CustomerID)
                .Index(t => t.Employee_EmployeeID)
                .Index(t => t.Vehicle_VehicleID)
                .Index(t => t.VehicleType_VehicleTypeID);
            
            CreateTable(
                "dbo.VehicleType",
                c => new
                    {
                        VehicleTypeID = c.Int(nullable: false, identity: true),
                        Descr = c.String(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UpdateDetailsUpdatedBy = c.String(),
                        UpdateDetailsUpdatedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.VehicleTypeID);
            
            CreateTable(
                "dbo.BookingBid",
                c => new
                    {
                        BookingBidID = c.Int(nullable: false, identity: true),
                        BookingID = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        VehicleID = c.Int(nullable: false),
                        EmployeeID = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UpdateDetailsUpdatedBy = c.String(),
                        UpdateDetailsUdpatedTime = c.DateTime(nullable: false),
                        Employee_EmployeeID = c.Long(),
                    })
                .PrimaryKey(t => t.BookingBidID)
                .ForeignKey("dbo.Booking", t => t.BookingID, cascadeDelete: true)
                .ForeignKey("dbo.Employee", t => t.Employee_EmployeeID)
                .ForeignKey("dbo.Vehicle", t => t.VehicleID, cascadeDelete: true)
                .Index(t => t.BookingID)
                .Index(t => t.VehicleID)
                .Index(t => t.Employee_EmployeeID);
            
            CreateTable(
                "dbo.BookingCancel",
                c => new
                    {
                        BookingCancelID = c.Int(nullable: false, identity: true),
                        BookingID = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        CancelledBy = c.Int(nullable: false),
                        CancelReason = c.String(),
                        VehicleID = c.Int(nullable: false),
                        EmployeeID = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UpdateDetailsUpdatedBy = c.String(),
                        UpdateDetailsUdpatedTime = c.DateTime(nullable: false),
                        Employee_EmployeeID = c.Long(),
                    })
                .PrimaryKey(t => t.BookingCancelID)
                .ForeignKey("dbo.Booking", t => t.BookingID, cascadeDelete: true)
                .ForeignKey("dbo.Employee", t => t.Employee_EmployeeID)
                .ForeignKey("dbo.Vehicle", t => t.VehicleID, cascadeDelete: true)
                .Index(t => t.BookingID)
                .Index(t => t.VehicleID)
                .Index(t => t.Employee_EmployeeID);
            
            CreateTable(
                "dbo.BookingPickupDrop",
                c => new
                    {
                        BookingPickupDropID = c.Int(nullable: false, identity: true),
                        BookingID = c.Int(nullable: false),
                        PickupAddress = c.String(),
                        DropAddress = c.String(),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UpdateDetailsUpdatedBy = c.String(),
                        UpdateDetailsUdpatedTime = c.DateTime(nullable: false),
                        DropMainArea_MainAreaID = c.Int(),
                        DropSubArea_SubAreaID = c.Int(),
                        PickupMainArea_MainAreaID = c.Int(),
                        PickupSubArea_SubAreaID = c.Int(),
                    })
                .PrimaryKey(t => t.BookingPickupDropID)
                .ForeignKey("dbo.Booking", t => t.BookingID, cascadeDelete: true)
                .ForeignKey("dbo.MainArea", t => t.DropMainArea_MainAreaID)
                .ForeignKey("dbo.SubArea", t => t.DropSubArea_SubAreaID)
                .ForeignKey("dbo.MainArea", t => t.PickupMainArea_MainAreaID)
                .ForeignKey("dbo.SubArea", t => t.PickupSubArea_SubAreaID)
                .Index(t => t.BookingID)
                .Index(t => t.DropMainArea_MainAreaID)
                .Index(t => t.DropSubArea_SubAreaID)
                .Index(t => t.PickupMainArea_MainAreaID)
                .Index(t => t.PickupSubArea_SubAreaID);
            
            CreateTable(
                "dbo.MainArea",
                c => new
                    {
                        MainAreaID = c.Int(nullable: false, identity: true),
                        MainAreaName = c.String(),
                    })
                .PrimaryKey(t => t.MainAreaID);
            
            CreateTable(
                "dbo.SubArea",
                c => new
                    {
                        SubAreaID = c.Int(nullable: false, identity: true),
                        MainAreaID = c.Int(nullable: false),
                        SubAreaName = c.String(),
                    })
                .PrimaryKey(t => t.SubAreaID)
                .ForeignKey("dbo.MainArea", t => t.MainAreaID, cascadeDelete: true)
                .Index(t => t.MainAreaID);
            
            CreateTable(
                "dbo.BookingRemarksAudit",
                c => new
                    {
                        BookingRemarksAuditID = c.Int(nullable: false, identity: true),
                        BookingID = c.Int(nullable: false),
                        Remarks = c.String(),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UpdateDetailsUpdatedBy = c.String(),
                        UpdateDetailsUdpatedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.BookingRemarksAuditID)
                .ForeignKey("dbo.Booking", t => t.BookingID, cascadeDelete: true)
                .Index(t => t.BookingID);
            
            CreateTable(
                "dbo.LiveTracking",
                c => new
                    {
                        LiveTrackingID = c.Long(nullable: false, identity: true),
                        VehicleNo = c.String(),
                        DateTime = c.DateTime(nullable: false),
                        Speed = c.Double(nullable: false),
                        Status = c.Int(nullable: false),
                        DriverId = c.String(),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        LoggedInStatus = c.Int(nullable: false),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.LiveTrackingID);
            
            //AddColumn("dbo.Customer", "CreatedBy", c => c.String());
            //AddColumn("dbo.Customer", "CreatedTime", c => c.DateTime(nullable: false));
            //AddColumn("dbo.Customer", "UpdateDetailsUpdatedBy", c => c.String());
            //AddColumn("dbo.Customer", "UpdateDetailsUpdatedTime", c => c.DateTime(nullable: false));
            //AddColumn("dbo.Customer", "CustomerType_CustomerTypeID", c => c.Long());
            AddColumn("dbo.CustomerType", "Descr", c => c.String(nullable: false));
            AddColumn("dbo.CustomerType", "CreatedBy", c => c.String());
            AddColumn("dbo.CustomerType", "CreatedTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.CustomerType", "UpdateDetailsUpdatedBy", c => c.String());
            AddColumn("dbo.CustomerType", "UpdateDetailsUpdatedTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.ItemCategory", "TimeStamp", c => c.DateTime());
            //AlterColumn("dbo.Customer", "CustomerTypeID", c => c.Int(nullable: false));
            AlterColumn("dbo.Vehicle", "ActiveStatus", c => c.Int(nullable: false));
            AlterColumn("dbo.Vehicle", "VehicleMakeID", c => c.Int(nullable: false));
            AlterColumn("dbo.Vehicle", "VehicleModelID", c => c.Int(nullable: false));
            AlterColumn("dbo.Vehicle", "VehicleTypeID", c => c.Int(nullable: false));
            AlterColumn("dbo.Vehicle", "OwnerID", c => c.Int(nullable: false));
            //CreateIndex("dbo.Customer", "CustomerType_CustomerTypeID");
            CreateIndex("dbo.Vehicle", "VehicleMakeID");
            CreateIndex("dbo.Vehicle", "VehicleModelID");
            CreateIndex("dbo.Vehicle", "VehicleTypeID");
            CreateIndex("dbo.Vehicle", "OwnerID");
            //AddForeignKey("dbo.Customer", "CustomerType_CustomerTypeID", "dbo.CustomerType", "CustomerTypeID");
            AddForeignKey("dbo.Vehicle", "OwnerID", "dbo.Owner", "OwnerID", cascadeDelete: true);
            AddForeignKey("dbo.Vehicle", "VehicleMakeID", "dbo.VehicleMake", "VehicleMakeID", cascadeDelete: true);
            AddForeignKey("dbo.Vehicle", "VehicleModelID", "dbo.VehicleModel", "VehicleModelID", cascadeDelete: true);
            AddForeignKey("dbo.Vehicle", "VehicleTypeID", "dbo.VehicleType", "VehicleTypeID", cascadeDelete: true);
            //DropColumn("dbo.Customer", "AlternativeNo");
            //DropColumn("dbo.Customer", "CorporateName");
            //DropColumn("dbo.Customer", "PartnerID");
            //DropColumn("dbo.Customer", "PartnerMemberCardNo");
            //DropColumn("dbo.Customer", "PickupAddress");
            //DropColumn("dbo.Customer", "PickupMainAreaID");
            //DropColumn("dbo.Customer", "PickupSubAreaID");
            //DropColumn("dbo.Customer", "UpdatedBy");
            //DropColumn("dbo.Customer", "UpdatedTime");
            DropColumn("dbo.CustomerType", "Customer_Type");
            DropColumn("dbo.CustomerType", "UpdatedBy");
            DropColumn("dbo.CustomerType", "UpdatedTime");
            DropColumn("dbo.ItemCategory", "UpdatedBy");
            DropColumn("dbo.ItemCategory", "UpdatedTime");
            DropColumn("dbo.Vehicle", "ColorID");
            DropColumn("dbo.Vehicle", "PlateNo");
            DropColumn("dbo.Vehicle", "VehicleNature");
            DropColumn("dbo.Vehicle", "MaxTankCapacity");
            DropColumn("dbo.Vehicle", "MaxVolt");
            DropColumn("dbo.Vehicle", "VehicleContactNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Vehicle", "VehicleContactNo", c => c.String(nullable: false));
            AddColumn("dbo.Vehicle", "MaxVolt", c => c.Int(nullable: false));
            AddColumn("dbo.Vehicle", "MaxTankCapacity", c => c.Int(nullable: false));
            AddColumn("dbo.Vehicle", "VehicleNature", c => c.String(nullable: false));
            AddColumn("dbo.Vehicle", "PlateNo", c => c.String(nullable: false));
            AddColumn("dbo.Vehicle", "ColorID", c => c.Long(nullable: false));
            AddColumn("dbo.ItemCategory", "UpdatedTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.ItemCategory", "UpdatedBy", c => c.Long(nullable: false));
            AddColumn("dbo.CustomerType", "UpdatedTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.CustomerType", "UpdatedBy", c => c.Long(nullable: false));
            AddColumn("dbo.CustomerType", "Customer_Type", c => c.String(nullable: false));
            AddColumn("dbo.Customer", "UpdatedTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customer", "UpdatedBy", c => c.Long(nullable: false));
            AddColumn("dbo.Customer", "PickupSubAreaID", c => c.Long(nullable: false));
            AddColumn("dbo.Customer", "PickupMainAreaID", c => c.Long(nullable: false));
            AddColumn("dbo.Customer", "PickupAddress", c => c.String());
            AddColumn("dbo.Customer", "PartnerMemberCardNo", c => c.String());
            AddColumn("dbo.Customer", "PartnerID", c => c.Long(nullable: false));
            AddColumn("dbo.Customer", "CorporateName", c => c.String());
            AddColumn("dbo.Customer", "AlternativeNo", c => c.String());
            DropForeignKey("dbo.BookingRemarksAudit", "BookingID", "dbo.Booking");
            DropForeignKey("dbo.BookingPickupDrop", "PickupSubArea_SubAreaID", "dbo.SubArea");
            DropForeignKey("dbo.BookingPickupDrop", "PickupMainArea_MainAreaID", "dbo.MainArea");
            DropForeignKey("dbo.BookingPickupDrop", "DropSubArea_SubAreaID", "dbo.SubArea");
            DropForeignKey("dbo.SubArea", "MainAreaID", "dbo.MainArea");
            DropForeignKey("dbo.BookingPickupDrop", "DropMainArea_MainAreaID", "dbo.MainArea");
            DropForeignKey("dbo.BookingPickupDrop", "BookingID", "dbo.Booking");
            DropForeignKey("dbo.BookingCancel", "VehicleID", "dbo.Vehicle");
            DropForeignKey("dbo.BookingCancel", "Employee_EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.BookingCancel", "BookingID", "dbo.Booking");
            DropForeignKey("dbo.BookingBid", "VehicleID", "dbo.Vehicle");
            DropForeignKey("dbo.BookingBid", "Employee_EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.BookingBid", "BookingID", "dbo.Booking");
            DropForeignKey("dbo.BookingAssign", "VehicleID", "dbo.Vehicle");
            DropForeignKey("dbo.BookingAssign", "Employee_EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.BookingAssign", "BookingID", "dbo.Booking");
            DropForeignKey("dbo.Booking", "VehicleType_VehicleTypeID", "dbo.VehicleType");
            DropForeignKey("dbo.Booking", "Vehicle_VehicleID", "dbo.Vehicle");
            DropForeignKey("dbo.Vehicle", "VehicleTypeID", "dbo.VehicleType");
            DropForeignKey("dbo.Vehicle", "VehicleModelID", "dbo.VehicleModel");
            DropForeignKey("dbo.Vehicle", "VehicleMakeID", "dbo.VehicleMake");
            DropForeignKey("dbo.Vehicle", "OwnerID", "dbo.Owner");
            DropForeignKey("dbo.Booking", "Employee_EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.Booking", "Customer_CustomerID", "dbo.Customer");
            DropForeignKey("dbo.Customer", "CustomerType_CustomerTypeID", "dbo.CustomerType");
            DropIndex("dbo.BookingRemarksAudit", new[] { "BookingID" });
            DropIndex("dbo.SubArea", new[] { "MainAreaID" });
            DropIndex("dbo.BookingPickupDrop", new[] { "PickupSubArea_SubAreaID" });
            DropIndex("dbo.BookingPickupDrop", new[] { "PickupMainArea_MainAreaID" });
            DropIndex("dbo.BookingPickupDrop", new[] { "DropSubArea_SubAreaID" });
            DropIndex("dbo.BookingPickupDrop", new[] { "DropMainArea_MainAreaID" });
            DropIndex("dbo.BookingPickupDrop", new[] { "BookingID" });
            DropIndex("dbo.BookingCancel", new[] { "Employee_EmployeeID" });
            DropIndex("dbo.BookingCancel", new[] { "VehicleID" });
            DropIndex("dbo.BookingCancel", new[] { "BookingID" });
            DropIndex("dbo.BookingBid", new[] { "Employee_EmployeeID" });
            DropIndex("dbo.BookingBid", new[] { "VehicleID" });
            DropIndex("dbo.BookingBid", new[] { "BookingID" });
            DropIndex("dbo.Vehicle", new[] { "OwnerID" });
            DropIndex("dbo.Vehicle", new[] { "VehicleTypeID" });
            DropIndex("dbo.Vehicle", new[] { "VehicleModelID" });
            DropIndex("dbo.Vehicle", new[] { "VehicleMakeID" });
            DropIndex("dbo.Customer", new[] { "CustomerType_CustomerTypeID" });
            DropIndex("dbo.Booking", new[] { "VehicleType_VehicleTypeID" });
            DropIndex("dbo.Booking", new[] { "Vehicle_VehicleID" });
            DropIndex("dbo.Booking", new[] { "Employee_EmployeeID" });
            DropIndex("dbo.Booking", new[] { "Customer_CustomerID" });
            DropIndex("dbo.BookingAssign", new[] { "Employee_EmployeeID" });
            DropIndex("dbo.BookingAssign", new[] { "VehicleID" });
            DropIndex("dbo.BookingAssign", new[] { "BookingID" });
            AlterColumn("dbo.Vehicle", "OwnerID", c => c.Long(nullable: false));
            AlterColumn("dbo.Vehicle", "VehicleTypeID", c => c.Long(nullable: false));
            AlterColumn("dbo.Vehicle", "VehicleModelID", c => c.Long(nullable: false));
            AlterColumn("dbo.Vehicle", "VehicleMakeID", c => c.Long(nullable: false));
            AlterColumn("dbo.Vehicle", "ActiveStatus", c => c.String(nullable: false));
            AlterColumn("dbo.Customer", "CustomerTypeID", c => c.Long(nullable: false));
            DropColumn("dbo.ItemCategory", "TimeStamp");
            DropColumn("dbo.CustomerType", "UpdateDetailsUpdatedTime");
            DropColumn("dbo.CustomerType", "UpdateDetailsUpdatedBy");
            DropColumn("dbo.CustomerType", "CreatedTime");
            DropColumn("dbo.CustomerType", "CreatedBy");
            DropColumn("dbo.CustomerType", "Descr");
            DropColumn("dbo.Customer", "CustomerType_CustomerTypeID");
            DropColumn("dbo.Customer", "UpdateDetailsUpdatedTime");
            DropColumn("dbo.Customer", "UpdateDetailsUpdatedBy");
            DropColumn("dbo.Customer", "CreatedTime");
            DropColumn("dbo.Customer", "CreatedBy");
            DropTable("dbo.LiveTracking");
            DropTable("dbo.BookingRemarksAudit");
            DropTable("dbo.SubArea");
            DropTable("dbo.MainArea");
            DropTable("dbo.BookingPickupDrop");
            DropTable("dbo.BookingCancel");
            DropTable("dbo.BookingBid");
            DropTable("dbo.VehicleType");
            DropTable("dbo.Booking");
            DropTable("dbo.BookingAssign");
        }
    }
}
